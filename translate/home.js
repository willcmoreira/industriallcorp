var textHome = {
	"home": {
		"btn_contact": {
			"pt": "contato",
			"en": "contact",
		},
		"fa_breve_text": {
			"pt": "Aprimore o relacionamento entre as áreas de manutenção e operação. Integre seus sistemas e tenha tudo em um único lugar. Melhore seu processo decisório e atinja resultados exponenciais.",
			"en": "Improve your operation and maintenance relationship. Integrate your systems and have all your plant information in one place. Take better decisions, achieve higher results.",
		},
		"scroll_mouse": {
			"pt": "rolar para baixo",
			"en": "scroll down",
		},
		"sis_boosting": {
			"pt": "Impulsionando o potencial industrial",
			"en": "Boosting industries potencial",
		},
		"sis_title_integrate": {
			"pt": "Integre seus sistemas",
			"en": "Integrate your systems",
		},
		"sis_text_lg_integrate": {
			"pt": "Acreditamos que a tecnologia e a criatividade humana têm o poder de mudar o ambiente industrial. É por isso que nos concentramos em facilitar o processo de integração de todos os pilares da Indústria 4.0. Para nós, a integração de sistemas é a chave para aproximar os humanos da tecnologia, porém, esse não é o único benefício. Entendemos que a integração também tem o poder de:",
			"en": "We believe that technology and human creativity has the power to change the industry field. That’s why we focus on facilitating the integration process of all Industry 4.0 pillars. For us, the integration system is the key to approach humans to technology, but this is not the unique benefit. We understand the integration has the power to:",
		},
		"sis_list_01": {
			"pt": "Padronizar processos;",
			"en": "Standardize processes;",
		},
		"sis_list_02": {
			"pt": "Aumentar a produtividade e eficiência;",
			"en": "Increase productivity and efficiency;",
		},
		"sis_list_03": {
			"pt": "Reduzir custos e perdas de produção;",
			"en": "Reduce costs and production losses;",
		},
		"sis_list_04": {
			"pt": "Aumentar a colaboração entre as áreas;",
			"en": "Increase colaboration between areas;",
		},
		"sis_list_05": {
			"pt": "Garantir a confiabilidade e integridade dos dados;",
			"en": "Guarantee data confiability and integrity;",
		},
		"sis_list_06": {
			"pt": "Aumentar a assertividade durante o processo de tomada de decisão;",
			"en": "Increase assertiviness during decision making process;",
		},
		"sis_text_this": {
			"pt": "É por isso que fazemos o que fazemos.",
			"en": "This is why we do what we do.",
		},
		"sis_btn_contact": {
			"pt": "Fale com um dos nossos especialistas",
			"en": "Talk to one of our specialists",
		},
		"p_nothing": {
			"pt": "Nada define melhor a revolução industrial que a evolução tecnológica.",
			"en": "Nothing defines an industrial revolution better than the technology involved.",
		},
		"title_pillars": {
			"pt": "Conheça os pilares da Industria 4.0",
			"en": "Get to know the pillars of Industry 4.0",
		},
		"title_user-friend": {
			"pt": "User friendly",
			"en": "User friendly",
		},
		"text_user_friend": {
			"pt": "O tema é responsivo, intuitivo e oferece uma navegação amigável. Seu trabalho parecerá bem mais fácil.",
			"en": "The theme is responsive, intuitive and offers a lean navigation. Your work will look easier.",
		},
		"title_combined": {
			"pt": "Combined efforts",
			"en": "Combined efforts",
		},
		"text_combined": {
			"pt": "Disponível para todos os níveis hierárquicos. Tenha diferentes telas e permissões de acordo com cada função.",
			"en": "Available for all hierarchical levels. Have different screens and permitions according to each role.",
		},
		"title_all_one": {
			"pt": "All in one",
			"en": "All in one",
		},
		"text_all_one": {
			"pt": "Nosso corebusiness é a integração do sistema. Seja mais rápido e mais assertivo em seu processo de tomada de decisão.",
			"en": "Our corebusiness is system integration. Be faster and more assertive on your decision making process.",
		},
		"title_team": {
			"pt": "Team oriented",
			"en": "Team oriented",
		},
		"text_team": {
			"pt": "Promove a comunicação entre as áreas industriais. Ajuda no relacionamento entre manutenção e operação, fazendo com que eles trabalhem juntos.",
			"en": "Improve your comunication between areas. Make your operation and maintenance work together.",
		},
		"title_data": {
			"pt": "RELIABLE DATA",
			"en": "RELIABLE DATA",
		},
		"text_data": {
			"pt": "Segurança, qualidade, rastreabilidade e precisão de dados. Trabalhe sem medo.",
			"en": "Security, quality, traceability and accuracy of information. Work fearless.",
		},
		"title_flexible": {
			"pt": "FLEXIBLE DEPLOYMENT",
			"en": "FLEXIBLE DEPLOYMENT",
		},
		"text_flexible": {
			"pt": "Escolha seus módulos. Parametrize tudo de acordo com a sua necessidade.",
			"en": "Choose your modules. Parametrize everything according to your needs.",
		},
		"title_joint": {
			"pt": "JOINT EVOLUTION",
			"en": "JOINT EVOLUTION",
		},
		"text_joint": {
			"pt": "Evoluções constantes por planejamento construído em conjunto com clientes. Juntos vamos mais longe.",
			"en": "Constant evolutions by roadmap built with clients. Together we go further.",
		},
		"title_multi": {
			"pt": "MULTI PLANTS",
			"en": "MULTI PLANTS",
		},
		"text_multi": {
			"pt": "Integre informações não só dos seus sistemas, mas de todas as plantas.Tenha tudo em mãos de maneira simples e fácil.",
			"en": "Integrate all your industrial plants information. Have everything easily on hands.",
		},
		"title_box_demo": {
			"pt": "SOLICITE UMA DEMONSTRAÇÃO",
			"en": "Request a demo",
		},
		"btn_box_demo": {
			"pt": "SOLICITAR",
			"en": "request",
		},
		"span_modules": {
			"pt": "módulos",
			"en": "modules",
		},
		"title_modules": {
			"pt": "Por que ALL in one?",
			"en": "Why ALL in one?",
		},
		"sub_executive": {
			"pt": "Dashboard Executivo",
			"en": "Executive Dashboard",
		},
		"text_executive": {
			"pt": "Acompanhe seus principais KPIs por meio de painéis configuráveis e flexíveis de gestão à vista. Decisões rápidas e assertivas necessitam de informações precisas em tempo real.",
			"en": "Track your KPIs through configurable and flexible sight management panels. Better and faster decisions require accurate information in real time.",
		},
		"sub_ocurrencies": {
			"pt": "Ocorrências",
			"en": "Ocurrencies",
		},
		"text_ocurrencies": {
			"pt": "Registre, acompanhe e controle os principais problemas e desvios que impactam em perdas de produção do seu processo industrial. A sinergia entre a operação e manutenção industrial é fundamental para alcançar altos níveis de produtividade.",
			"en": "Record, monitor and control the main problems and deviations that impact the loss of industrial production. The synergy between the operation and the industrial maintenance is fundamental for the increase of high levels of productivity.",
		},
		"sub_forces": {
			"pt": "Forces",
			"en": "Forces",
		},
		"text_forces": {
			"pt": "Monitore mudanças no controle automático/manual de equipamentos da área industrial. A gestão de forces contribui significativamente para a manutenção de um ambiente industrial seguro.",
			"en": "Monitor changes in automatic / manual control of industrial equipment. Forces management is important for maintaining a safe industrial environment.",
		},
		"sub_minutes": {
			"pt": "Atas",
			"en": "Minutes",
		},
		"text_minutes": {
			"pt": "Registre, acompanhe e controle atas de reunião e suas ações de modo simples e amigável. Simplicidade e facilidade de uso são fundamentais para uma rotina eficiente.",
			"en": "Record, track and control meeting minutes and their actions in a simple and user-friendly manner. Simplicity and ease of use are fundamental to an efficient routine.",
		},
		"sub_p_control": {
			"pt": "Controle de Processo",
			"en": "Process Control",
		},
		"text_p_control": {
			"pt": "Monitore suas principais variáveis de controle e verificação de forma integrada com seus sistemas industriais. A estabilidade do processo garante melhores resultados operacionais.",
			"en": "Monitor your key control and verification variables in an integrated way with your industrial systems. Stability of the process ensures better operating results.",
		},
		"sub_improvoments": {
			"pt": "Melhorias",
			"en": "Improvements",
		},
		"text_improvoments": {
			"pt": "Conduza a análise e tratativa de problemas críticos a partir de um ciclo PDCA guiado step by step. A maturidade de uma planta se mede pela baixa recorrência de problemas.",
			"en": "Conduct analysis and handling of critical problems from a step by step guided cycle. The maturity of a plant is measured by the low recurrence of problems.",
		},
		"text_depo": {
			"pt": "Nosso diferencial vai além da integração de sistemas. Desenvolvemos nossa plataforma ouvindo as reais necessidades de nossos clientes. Sempre entregamos o que prometemos, agregando valor ao cliente final. Integrar, entregar e agregar. Essa é a base de todos os nossos esforços.",
			"en": "Our differential goes beyond system integration. We evolve our platform listening to the real needs of our clients. We always deliver what we promise, adding value to the end client. Integrate, cosolidate and aggregate. That is the basis of all our efforts.",
		},
		"title_partners": {
			"pt": "Quem está conosco?",
			"en": "Who is with us?",
		},
		"text_eldorado": {
			"pt": "A Eldorado é uma empresa que sempre cresceu em resultados, eficiência e produção. Nossos números são extremamente competitivos e, na maioria das vezes, baseado no gerenciamento pelo IndustriALL. A plataforma não é apenas uma tecnologia para nós; é a base de nossa indústria e demonstra como transcrevemos nossa gestão. Como gerente de controle técnico e processos, considero o IndustriALL é uma de nossas principais plataformas de gerenciamento industrial na Eldorado.",
			"en": "Eldorado is a company that has always grown in results, efficiency and production. Our numbers are extremely competitive and, most of the time, based on IndustriALL management. The platform is not only a technology for us; it is the basis of our industry and demonstrates how we transcribe our management. As a manager of technical control and processes, I consider IndustriALL to be one of our main industrial management platforms at Eldorado.",
		},
		"span_prof_eldorado": {
			"pt": "Gerente Técnico",
			"en": "Technical Manager",
		},
		"title_blog": {
			"pt": "O que está acontecendo? Conheça nosso blog",
			"en": "What is new? Get to know our blog",
		},
		"title_touch": {
			"pt": "Fale conosco!",
			"en": "Keep in touch",
		},
		"text_touch": {
			"pt": "Envie uma mensagem e você será respondido o mais rapido possível!",
			"en": "Send a message and you will hear from us as soon as possible!",
		},
		"btn_touch": {
			"pt": "Contato",
			"en": "Contact",
		},
		"menu_start": {
			"pt": "início",
			"en": "start",
		},
		"menu_purpose": {
			"pt": "propósito",
			"en": "purpose",
		},
		"menu_features": {
			"pt": "características",
			"en": "features",
		},
		"menu_modules": {
			"pt": "módulos",
			"en": "modules",
		},
		"menu_clients": {
			"pt": "clientes",
			"en": "clients",
		},
		"menu_news": {
			"pt": "blog",
			"en": "blog",
		},
		"title_sidebar": {
			"pt": "Estamos para operação",
			"en": "We are for operation",
		},
		"text_sidebar": {
			"pt": "Aprimore o relacionamento entre as áreas de manutenção e operação. Integre seus sistemas e tenha tudo em um único lugar. Melhore seu processo decisório e atinja resultados exponenciais.",
			"en": "Improve your operations and maintenance relationship. Integrate your systems and have all your plant information in one place. Take better decisions, achieve higher results.",
		},
		"text_veracel": {
			"pt": "A AEVO é uma empresa muito parceira, trabalha junto e sempre se adequa a necessidade do cliente. A principal transformação que o IndustriALL trouxe para a Veracel foi a consolidação de todas as informações em uma única plataforma. Isso proporcionou mais agilidade no gerenciamento das informações e no controle de perdas da produção, fatores que são extremamente importantes para a tomada de decisão dos gestores.",
			"en": "The main transformation that the IndustriALL brought to Veracel was the consolidation of all the information in a single platform, as well as more agility in the management of the information and control of production losses, factors that are extremely important for the manager’s decision making process. AEVO is a very partner company, work together with us and always suits the needs of the client.",
		},
		"span_prof_veracel": {
			"pt": "Analista de Sistemas de Automação Industrial",
			"en": "Industrial Automation Systems Analyst",
		},
		"text_lwarcel": {
			"pt": "Com o uso da plataforma, a Bracell se tornou mais ágil na tomada de decisões. O IndustriALL nos ajudou em uma mudança de cultura onde melhoramos a qualidade das informações que colocamos no sistema. Os principais benefícios do IndustriALL são a interface amigável, que colabora para a praticidade no lançamento de ocorrências na plataforma, e a integração com os demais sistemas, visto que antes tínhamos as informações em lugares diferentes.",
			"en": "While using the platform, Bracell has become more agile in making decisions. IndustriALL has helped us in a change of culture where we have improved the quality of the information we put into the system. The main benefit of IndustriALL is the user-friendly interface that contributes to the practicality of launching occurrences in the system, as well as being able to integrate some information that we used to have in separate places.",
		},
		"span_prof_lwarcel": {
			"pt": "Coordenador de Produção",
			"en": "Production Coordinator",
		},
		"title_page_contact": {
			"pt": "Pronto para uma demonstração?",
			"en": "Ready for a live demo?",
		},
		"text_page_contact": {
			"pt": "Vamos conversar sobre como integrar os seus sistemas e centralizar todas as informações de suas plantas. Estamos curiosos sobre o quanto isso pode melhorar o relacionamento entre suas áreas industriais.",
			"en": "Let's talk about integrating your systems and help you to have all your plant information in one place. We are pumped about what we can do for your maintenance and operations relationship!",
		},
		"span_case": {
			"pt": "case de sucesso:",
			"en": "success case:",
		},
		"title_box_form": {
			"pt": "Preencha o formulário abaixo e em breve entraremos em contato!",
			"en": "Fill the form bellow and you will hear from us soon!",
		},
		"label_name": {
			"pt": "Nome",
			"en": "Name",
		},
		"label_email": {
			"pt": "Email",
			"en": "Email",
		},
		"label_company": {
			"pt": "Empresa",
			"en": "Company",
		},
		"label_current": {
			"pt": "Cargo atual",
			"en": "Current role",
		},
		"p_form": {
			"pt": "Como posso ajudá-lo?",
			"en": "How can I help you?",
		},
		"label_message": {
			"pt": "Mensagem",
			"en": "Message",
		},
		"send": {
			"pt": "Enviar",
			"en": "Send",
		},
		"text_all": {
			"pt": "100%",
			"en": "All",
		},
		"text_mod_imp": {
			"pt": "dos módulos implementados",
			"en": "Modules Implemented",
		},
		"text_chip": {
			"pt": "Aumento no preparo de cavacos",
			"en": "Chip Preparation Increase",
		},
		"text_record": {
			"pt": "Recorde histórico na produção de cavacos",
			"en": "Record of chip production",
		},
		"title_big_data": {
			"pt": "Big data e análises",
			"en": "Big Data and Analytics",
		},
		"text_big_data": {
			"pt": "No contexto da Industria 4.0, a coleta e avaliação abrangente de dados de diversos sistemas e equipamentos de produção, bem como sistemas de gerenciamento de clientes e empresas, se tornará padrão para apoiar a tomada de decisões em tempo real.",
			"en": "In an Industry 4.0 context, the collection and comprehensive evaluation of data from many different sources—production equipment and systems as well as enterprise- and customer-management systems—will become standard to support real-time decision making.",
		},
		"title_auto_roots": {
			"pt": "Robôs autônomos",
			"en": "Autonomous Robots",
		},
		"text_auto_roots": {
			"pt": "Os robôs irão interagir uns com os outros, trabalhando em segurança lado a lado com os humanos e aprendendo com eles. Esses robôs custarão menos e terão uma gama maior de recursos do que os usados atualmente nas indústrias.",
			"en": "Robots will eventually interact with one another and work safely side by side with humans and learn from them. These robots will cost less and have a greater range of capabilities than those used in manufacturing today.",
		},
		"title_simulation": {
			"pt": "Simulação",
			"en": "Simulation",
		},
		"text_simulation": {
			"pt": "As simulações serão usadas mais nas operações de fábrica para alavancar dados em tempo real e espelhar o mundo físico em um modelo virtual, que pode incluir máquinas, produtos e seres humanos. Isso permitirá que os operadores testem e otimizem as configurações da máquina para o próximo produto em linha no mundo virtual antes da troca física, reduzindo assim os tempos de configuração da máquina e aumentando a qualidade do serviço e ou produção.",
			"en": "Simulations will be used more extensively in plant operations to leverage real-time data and mirror the physical world in a virtual model, which can include machines, products, and humans. This will allow operators to test and optimize the machine settings for the next product in line in the virtual world before the physical changeover, thereby driving down machine setup times and increasing quality.",
		},
		"title_horizontal": {
			"pt": "Integração horizontal e vertical",
			"en": "Horizontal and Vertical System Integration",
		},
		"text_horizontal": {
			"pt": "Com a Industria 4.0, as empresas, departamentos, funções e capacidades se tornarão muito mais coesas, à medida que as redes universais de integração de dados entre empresas evoluírem e permitirem cadeias de valor verdadeiramente automatizadas.",
			"en": "With Industry 4.0, companies, departments, functions, and capabilities will become much more cohesive, as cross-company, universal data-integration networks evolve and enable truly automated value chains.",
		},
		"title_things": {
			"pt": "Internet das Coisas Industrial",
			"en": "Industrial Internet of Things",
		},
		"text_things": {
			"pt": "Permite a conectividade entre dispositivos, tornando o acesso e o controle flexíveis durante todo o processo de produção.",
			"en": "Allows connectivity between devices making access and control flexible throughout the production process.",
		},
		"title_cyber": {
			"pt": "Segurança cibernética",
			"en": "Cybersecurity",
		},
		"text_cyber": {
			"pt": "Com o aumento da conectividade e o uso de protocolos de comunicação padrão que vêm com a indústria 4.0, a necessidade de proteger sistemas industriais críticos e linhas de fabricação de ameaças de segurança cibernética, aumenta dramaticamente. Como resultado, comunicações seguras e confiáveis, bem como gerenciamento sofisticado de identidade e acesso de máquinas e usuários são essenciais.",
			"en": "With the increased connectivity and use of standard communications protocols that come with Industry 4.0, the need to protect critical industrial systems and manufacturing lines from cybersecurity threats increases dramatically. As a result, secure, reliable communications as well as sophisticated identity and access management of machines and users are essential.",
		},
		"title_cloud": {
			"pt": "Computação em nuvem",
			"en": "Cloud Computing",
		},
		"text_cloud": {
			"pt": "Facilitará o acesso ao banco de dados e o suporte de qualquer local do planeta, permitindo a integração de sistemas e instalações em locais distintos. Da mesma forma, o controle e o suporte poderão ser efetuados também de maneira global.",
			"en": "More production-related undertakings will require increased data sharing across sites and company boundaries. At the same time, the performance of cloud technologies will improve, achieving reaction times of just several milliseconds. As a result, machine data and functionality will increasingly be deployed to the cloud, enabling more data-driven services for production systems.",
		},
		"title_add_manu": {
			"pt": "Manufatura aditiva",
			"en": "Additive Manufacturing",
		},
		"title_featured": {
			"pt": "Características da plataforma",
			"en": "Get to know our features",
		},
		"text_add_manu": {
			"pt": "As empresas começaram a utilizar impressoras 3D principalmente para prototipar e produzir componentes individuais. Com a Indústria 4.0, esses métodos de fabricação de aditivos serão amplamente utilizados para produzir pequenos lotes de produtos personalizados que ofereçam vantagens de construção, como designs complexos e ao mesmo tempo, leves.",
			"en": "Companies have just begun to adopt additive manufacturing, such as 3-D printing, which they use mostly to prototype and produce individual components. With Industry 4.0, these additive-manufacturing methods will be widely used to produce small batches of customized products that offer construction advantages, such as complex, lightweight designs.",
		},
		"title_vr": {
			"pt": "Realidade aumentada",
			"en": "Augmented Reality",
		},
		"text_vr": {
			"pt": "Os sistemas baseados em realidade aumentada suportam uma variedade de serviços, como a seleção de peças em um armazém e o envio de instruções de reparo em dispositivos móveis. Esses sistemas estão atualmente em fase embrionária, mas, no futuro, as empresas usarão muito mais a realidade aumentada para fornecer aos funcionários informações em tempo real para melhorar a tomada de decisões e os procedimentos de trabalho.",
			"en": "Augmented-reality-based systems support a variety of services, such as selecting parts in a warehouse and sending repair instructions over mobile devices. These systems are currently in their infancy, but in the future, companies will make much broader use of augmented reality to provide workers with real-time information to improve decision making and work procedures.",
		},
		"title-resp-success": {
			"pt": "Email enviado com sucesso.",
			"en": "Email successfully sent.",
		},
		"text-resp-success": {
			"pt": "Em breve entraremos em contato!",
			"en": "Soon we will contact you!",
		},
		"btn-back": {
			"pt": "voltar",
			"en": "back",
		},
		"btn-contact-purpose": {
			"pt": "Fale conosco",
			"en": "Contact us",
		},
		"text_suzano": {
			"pt": "Atualmente temos uma operação bastante enxuta e dinâmica, os sistemas que nos apoiam na gestão da produção tem que acompanhar esse mesmo dinamismo. Estamos encontrando na AEVO um parceiro disposto a customizar seus sistemas para as nossas necessidades, agilizando assim a tomada de decisão nos nossos processos. Entendemos que decisões ágeis e assertivas são fundamentais para tornar os nossos resultados ainda melhores.",
			"en": "Currently we have a rather lean and dynamic operation and the systems that support us in the management of production must comply with this same dynamism. We have found in AEVO a partner willing to customize their systems to our needs, thus streamlining our decision making process. We believe that agile and assertive decisions are critical to making our results even better.",
		},
		"span_prof_suzano": {
			"pt": "Consultor de Processos Industriais",
			"en": "Consultant of Industrial Process",
		},
	}
};
