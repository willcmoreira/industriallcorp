<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$mail = new PHPMailer(true);
try {
    //Server settings
    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreplyinsany@gmail.com';                 // SMTP username
    $mail->Password = 'insany-dev';                             // SMTP password
    $mail->SMTPSecure = 'tls';
    $mail->CharSet = 'UTF-8';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('contato@industriallcorp.com', 'Contato do Site - AEVO INDUSTRIALL');
    $mail->addAddress('contato@industriallcorp.com', 'Contato do Site - AEVO INDUSTRIALL');     // Add a recipient
    //$mail->addReplyTo('email@email.com', 'Information');

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'AEVO INDUSTRIALL - Contato do Site';
    $mail->Body    = "
    <html><body>
        <p>Nome: ".$_POST['name']."</p>
        <p>E-mail: ".$_POST['email']."</p>
        <p>Empresa: ".$_POST['company']."</p>
        <p>Cargo Atual: ".$_POST['current']."</p>
        <p>Mensagem: ".$_POST['message']."</p>
    </body></html>";

    $enviado = $mail->send();
    // echo 'Message has been sent';
    if($enviado){
        echo json_encode('true');
    }else{
        echo json_encode('false');
    }
} catch (Exception $e) {
    echo json_encode('error'); //'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}