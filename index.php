<!doctype html>
<html class="no-js" lang="">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>INDUSTRIALL | SOFTWARE DE GESTÃO INDUSTRIAL</title>
    <meta name="title" content="SOFTWARE PARA GESTÃO DA INOVAÇÃO" />
    <meta name="description"
        content="Transforme ideias inovadoras dos seus colaboradores em resultados e crie a cultura da inovação na sua empresa com a melhor plataforma de inovação organizacional do mercado." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="global" />
    <meta name="rating" content="general" />
    <meta name="robots" content="ALL" />
    <meta name="language" content="pt-br" />
    <meta name="author" content="Backstage Digital" />

    <meta property='og:title' content="AEVO INNOVATE | SOFTWARE PARA GESTÃO DA INOVAÇÃO" />
    <meta property='og:description'
        content="Transforme ideias inovadoras dos seus colaboradores em resultados e crie a cultura da inovação na sua empresa com a melhor plataforma de inovação organizacional do mercado." />
    <meta property='og:url' content="http://www.aevo.com.br/" />
    <link rel="icon" type="image/png" href="favicon.png" />
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/main.min.css">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

    })(window,document,'script','dataLayer','GTM-KNFH9PJ');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNFH9PJ"

    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->
    <header class="section" id="start">
        <div class="container">
            <div class="logo">
                <a href="">
                    <div class="logo-desk" data-piio-bck="img/opt/logo-black.png"></div>
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/logo-mob.png" alt="Logo Aevo Industriall" class="logo-mob">
                </a>
            </div>
            <nav>
                <div class="language">
                    <a href="" class="drop-selected">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/flag-en.svg" class="flag" alt="Flag EUA">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-down.svg" class="arrow-down" alt="">
                    </a>
                    <div class="drop-lang">
                        <ul>
                            <li><a href="#" id="portuguese"><img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/flag-pt-br.svg" alt="Flag BRASIL"></a></li>
                            <li><a href="#" id="english"><img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/flag-en.svg" alt=" Flag EUA"></a></li>
                        </ul>
                    </div>
                </div>
                <ul class="contact">
                    <li><a href="contato.html" id="btn-contact" data-i18n="home.btn_contact">contact</a></li>
                </ul>
                <a id="js-open-menu" class="menu-button">
                    <i class="menu-icon"></i>
                </a>
            </nav>
        </div>
    </header>

    <div class="menu-mobile">
        <div class="menu-titulo">
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/logo-preta.png" class="logo" alt="Logo IndustriALL">
            <p data-i18n="home.text_sidebar">Improve your operations and maintenance relationship. Integrate your
                systems and have all your plant
                information in one place. Take better decisions, achieve higher results.</p>
        </div>
        <ul>
            <li>
                <a href="#start" data-i18n="home.menu_start">Start</a>
            </li>
            <li>
                <a href="#purpose" data-i18n="home.menu_purpose">Purpose</a>
            </li>
            <li>
                <a href="#features" data-i18n="home.menu_features">Features</a>
            </li>
            <li>
                <a href="#modules" data-i18n="home.menu_modules">Modules</a>
            </li>
            <li>
                <a href="#clients" data-i18n="home.menu_clients">Clients</a>
            </li>
            <li>
                <a href="#news" data-i18n="home.menu_news">News</a>
            </li>
            <li>
                <a href="contato.html" id="btn-contact-menu" data-i18n="home.btn_contact">Contact</a>
            </li>
        </ul>

        <div class="redes">
            <a href="https://www.instagram.com/aevo.ti/" target="_blank"><i class="fa fa-instagram"></i></a>
            <a href="https://www.facebook.com/AEVOTI/" target="_blank"><i class="fa fa-facebook-square"></i></a>
            <a href="https://twitter.com/AevoTI" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.linkedin.com/company/aevo-ti/" target="_blank"><i class="fa fa-linkedin"></i></a>
            <a href="https://www.youtube.com/channel/UC6elpZXXa2TMa5kvrNgC-yQ" target="_blank"><i
                    class="fa fa-youtube"></i></a>
        </div>

        <span class="text-direitos">© AEVO - 2019. Todos os direitos reservados.</span>
    </div>

    <section class="featured-area" data-piio-bck="img/opt/bg-s-featured.jpg">
        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/chart.svg" class="chart" alt="" data-aos="fade-in" data-aos-duration="2000" data-aos-once="true">
        <div class="container">
            <div class="featured-text" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <h2>we are for</h2>
                <h1 class="rw-words rw-words-1">
                    <span>operation</span>
                    <span>maintenance</span>
                    <span>decision makers</span>
                    <span>operation</span>
                    <span>maintenance</span>
                    <span>decision makers</span>
                </h1>
                <p data-i18n="home.fa_breve_text">
                    Improve your operation and maintenance relationship. Integrate your systems and have all your plant
                    information in one place. Take better decisions, achieve higher results.
                </p>
                <div class="mouse">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/icon-mouse.svg" class="icon" alt="">
                    <span data-i18n="home.scroll_mouse">scroll down</span>
                </div>
            </div>
            <div class="text-integration section" id="purpose">
                <div class="text" data-aos="fade-right" data-aos-duration="1000" data-aos-once="true">
                    <strong>our purpose</strong>
                    <h4>We strive to be the</h4>
                    <h2>integration management</h2>
                    <div class="info">
                        <span class="dot-desk">. . . . . . . . . . . </span>
                        <span class="dot-mob">. . . . . . . . </span>
                        <br>
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/combined-shape-copy.svg" alt="" class="arrow-mob">
                        <div class="plataform">
                            <h3>platform,</h3>
                            <p>reference for industry 4.0</p>
                        </div>
                    </div>
                    <a href="contato.html" class="btn-contact" id="btn-contact-purpose">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-69.svg" alt="">
                        <span data-i18n="home.btn-contact-purpose">Contact us</span>
                    </a>
                </div>
                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/corpo-todo-recortado.png" class="peoples" alt="Imagem de duas pessoas conversando"
                    data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">
            </div>
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/combined-shape-copy.svg" alt="" class="arrow">
        </div>
    </section>

    <section class="s-integrate-system">
        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/effect.svg" class="effect">
        <div class="container">
            <div class="ilustration" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <div class="box-blue">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/logo-white.png" alt="">
                    <p data-i18n="home.sis_boosting">Boosting industries potencial</p>
                </div>
                <div id="motion"></div>
            </div>
            <div class="text" data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">
                <h2 data-i18n="home.sis_title_integrate">Integrate your systems </h2>
                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/pontilhado.svg" class="line" alt="">
                <p data-i18n="home.sis_text_lg_integrate">
                    We believe that technology and human creativity has the power to change the industry field. That’s
                    why we focus on facilitating the integration process of all Industry 4.0 pillars. For us, the
                    integration system is the key to approach humans to technology, but this is not the unique benefit.
                    We understand the integration has the power to:

                </p>
                <ul>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_01">Standardize processes;</span>
                    </li>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_02">Increase productivity and efficiency;</span>
                    </li>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_03">Reduce costs and production losses;</span>
                    </li>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_04">Increase colaboration between areas;.</span>
                    </li>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_05">Guarantee data confiability and integrity;</span>
                    </li>
                    <li>
                        <div class="circle"></div>
                        <span data-i18n="home.sis_list_06">Increase assertiviness during decision making process;</span>
                    </li>
                </ul>

                <a href="contato.html" id="btn-contact-us"><img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-69.svg" alt=""> <span
                        data-i18n="home.sis_btn_contact">contact us</span></a>
            </div>
        </div>
    </section>

    <section class="s-pillars" data-piio-bck="img/opt/bg-pillars.svg">
        <div class="container">
            <div class="text-info" data-aos="fade-right" data-aos-duration="1000" data-aos-once="true">
                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-circle.svg" alt="">
                <p data-i18n="home.p_nothing">
                    Nothing defines an industrial revolution better than the technology involved.
                </p>
            </div>
            <div class="text-geral" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <h2 data-i18n="home.title_pillars">Get to know the pillars of Industry 4.0</h2>
                <ul>
                    <li class="active" id="cont-big-data">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_big_data">Big Data and Analytics</h3>
                        </div>
                        <p data-i18n="home.text_big_data">
                            In an Industry 4.0 context, the collection and comprehensive evaluation of data from many
                            different sources—production equipment and systems as well as enterprise- and
                            customer-management systems—will become standard to support real-time decision making.
                        </p>
                    </li>
                    <li id="cont-auto-root">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_auto_roots">Autonomous Robots</h3>
                        </div>
                        <p data-i18n="home.text_auto_roots">
                            Robots will eventually interact with one another and work safely side by side with humans
                            and learn from them. These robots will cost less and have a greater range of capabilities
                            than those used in manufacturing today.
                        </p>
                    </li>
                    <li id="cont-simulation">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_simulation">Simulation</h3>
                        </div>
                        <p data-i18n="home.text_simulation">
                            Simulations will be used more extensively in plant operations to leverage real-time data and
                            mirror the physical world in a virtual model, which can include machines, products, and
                            humans. This will allow operators to test and optimize the machine settings for the next
                            product in line in the virtual world before the physical changeover, thereby driving down
                            machine setup times and increasing quality.
                        </p>
                    </li>
                    <li id="cont-horizontal">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_horizontal">Horizontal and Vertical System Integration</h3>
                        </div>
                        <p data-i18n="home.text_horizontal">
                            With Industry 4.0, companies, departments, functions, and capabilities will become much more
                            cohesive, as cross-company, universal data-integration networks evolve and enable truly
                            automated value chains.
                        </p>
                    </li>
                    <li id="cont-industrial">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_things">The Industrial Internet of Things</h3>
                        </div>
                        <p data-i18n="home.text_things">
                            Allows connectivity between devices making access and control flexible throughout the
                            production process;
                        </p>
                    </li>
                    <li id="cont-cyber">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_cyber">Cybersecurity</h3>
                        </div>
                        <p data-i18n="home.text_cyber">
                            With the increased connectivity and use of standard communications protocols that come with
                            Industry 4.0, the need to protect critical industrial systems and manufacturing lines from
                            cybersecurity threats increases dramatically. As a result, secure, reliable communications
                            as well as sophisticated identity and access management of machines and users are essential.
                        </p>
                    </li>
                    <li id="cont-cloud">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_cloud">The Cloud</h3>
                        </div>
                        <p data-i18n="home.text_cloud">
                            More production-related undertakings will require increased data sharing across sites and
                            company boundaries. At the same time, the performance of cloud technologies will improve,
                            achieving reaction times of just several milliseconds. As a result, machine data and
                            functionality will increasingly be deployed to the cloud, enabling more data-driven services
                            for production systems.
                        </p>
                    </li>
                    <li id="cont-additive">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_add_manu">Additive Manufacturing</h3>
                        </div>
                        <p data-i18n="home.text_add_manu">
                            Companies have just begun to adopt additive manufacturing, such as 3-D printing, which they
                            use mostly to prototype and produce individual components. With Industry 4.0, these
                            additive-manufacturing methods will be widely used to produce small batches of customized
                            products that offer construction advantages, such as complex, lightweight designs.
                        </p>
                    </li>
                    <li id="cont-real">
                        <div class="title">
                            <div class="circle"></div>
                            <h3 data-i18n="home.title_vr">Augmented Reality</h3>
                        </div>
                        <p data-i18n="home.text_vr">
                            Augmented-reality-based systems support a variety of services, such as selecting parts in a
                            warehouse and sending repair instructions over mobile devices. These systems are currently
                            in their infancy, but in the future, companies will make much broader use of augmented
                            reality to provide workers with real-time information to improve decision making and work
                            procedures.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="pillars-geral" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true">
            <div class="oval animated infinite pulse" data-piio-bck="img/opt/oval.svg"></div>
            <div class="circle-logo">
                <a href="./contato.html">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/logo-vertical.png" alt="Logo Aevo Industrial">
                </a>
            </div>
            <ul>
                <li id="btn-auto-root">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-11-copy.svg" alt="Autonomous Robots">
                </li>
                <li id="btn-simulation">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-22-copy.svg" alt="Simulation">
                </li>
                <li id="btn-horizontal">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-29.svg" alt="Horizontal and Vertical System Integration">
                </li>
                <li id="btn-industrial">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-19.svg" alt="The Industrial Internet of Things">
                </li>
                <li id="btn-cyber">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/combined-shape-copy-3.svg" alt="Cybersecurity">
                </li>
                <li id="btn-cloud">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-37.svg" alt="The Cloud">
                </li>
                <li id="btn-additive">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-34.svg" alt="Additive Manufacturing">
                </li>
                <li id="btn-real">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-57.svg" alt="Augmented Reality">
                </li>
                <li class="active" id="btn-big-data">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-14.svg" alt="Big Data and Analytics">
                </li>
            </ul>
        </div>
    </section>

    <section class="s-featured section" id="features" data-piio-bck="img/opt/bg-featured.jpg">
        <div class="container">
            <h1 data-aos="fade-down" data-aos-duration="1000" data-aos-once="true" data-i18n="home.title_featured">Get
                to know our features</h1>
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/line-01.svg" class="line" alt="" data-aos="fade-down" data-aos-duration="1000"
                data-aos-once="true">
            <div class="slide-featured" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/device-2.svg" alt="User friendly">
                            <h2 data-i18n="home.title_user-friend">User friendly</h2>
                            <p data-i18n="home.text_user_friend">
                                The theme is responsive, intuitive and offers a lean navigation. your work will look
                                easier.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/group-56.svg" alt="Combined efforts">
                            <h2 data-i18n="home.title_combined">Combined efforts</h2>
                            <p data-i18n="home.text_combined">
                                Available for all hierarchical levels. Have different screens and permitions according
                                to each role.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/device-2-copy-2.svg" alt="All in one">
                            <h2 data-i18n="home.title_all_one">All in one</h2>
                            <p data-i18n="home.text_all_one">
                                Our corebusiness is system integration. Be faster and more assertive on your decision
                                making process.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/device-2-copy-3.svg" alt="Team oriented">
                            <h2 data-i18n="home.title_team">Team oriented</h2>
                            <p data-i18n="home.text_team">
                                Improve your comunication between areas. Make your operation and maintenance work
                                together.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/icon-card-08-reliable.svg" alt="RELIABLE DATA">
                            <h2 data-i18n="home.title_data">RELIABLE DATA</h2>
                            <p data-i18n="home.text_data">
                                Security, quality, traceability and accuracy of information. Work fearless.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/icon-card-07-flexible.svg" alt="FLEXIBLE DEPLOYMENT">
                            <h2 data-i18n="home.title_flexible">FLEXIBLE DEPLOYMENT</h2>
                            <p data-i18n="home.text_flexible">
                                Choose your modules. Parametrize everything according to your needs.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/icon-card-06-joint.svg" alt="JOINT EVOLUTION">
                            <h2 data-i18n="home.title_joint">JOINT EVOLUTION</h2>
                            <p data-i18n="home.text_joint">
                                Constant evolutions by roadmap built with clients. Together we go further.
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-white">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/icon-card-05-multi-plants.svg" alt="MULTI PLANTS">
                            <h2 data-i18n="home.title_multi">MULTI PLANTS</h2>
                            <p data-i18n="home.text_multi">
                                Integrate all your industrial plants information. Have everything easily on hands.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-prev.svg" class="btn btn-prev" alt="">
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-next.svg" class="btn btn-next" alt="">
        </div>
    </section>

    <section class="s-modules">
        <div class="container">
            <div class="box-demo" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true" data-piio-bck="img/opt/bg-demo.png">
                <div class="title">
                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-circle-02.svg" alt="">
                    <h2 data-i18n="home.title_box_demo">Request a demo</h2>
                </div>
                <a href="contato.html" class="btn" id="btn-request" data-i18n="home.btn_box_demo">request</a>
            </div>
            <span class="subtitle" data-aos="fade-down" data-aos-duration="1000" data-aos-once="true"
                data-i18n="home.span_modules">modules</span>
            <h3 class="section"  id="modules" data-aos="fade-down" data-aos-duration="1000" data-aos-once="true" data-i18n="home.title_modules">Why
                ALL in one?</h3>
            <div class="area-slide">
                <div class="slide-screens" data-aos="fade-right" data-aos-duration="1000" data-aos-once="true">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_01.svg" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_02.svg" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_05.svg" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_03.svg" alt="">
                        </div>

                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_06.svg" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_04.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="slide-itens-modules" data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/web-grid-alt.svg" alt="">
                                    <h4 data-i18n="home.sub_executive">Executive Dashboard</h4>
                                </div>
                                <p data-i18n="home.text_executive">
                                    Track your KPIs through configurable and flexible sight management panels. Better
                                    and faster decisions require accurate information in real time.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_01.svg" class="img-mob" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/file-medical-alt.svg" alt="">
                                    <h4 data-i18n="home.sub_ocurrencies">Ocurrencies</h4>
                                </div>
                                <p data-i18n="home.text_ocurrencies">
                                    Record, monitor and control the main problems and deviations that impact the loss of
                                    industrial production. The synergy between the operation and the industrial
                                    maintenance is fundamental for the increase of high levels of productivity.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_02.svg" class="img-mob" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/bolt-alt.svg" alt="">
                                    <h4 data-i18n="home.sub_forces">Forces</h4>
                                </div>
                                <p data-i18n="home.text_forces">
                                    Monitor changes in automatic / manual control of industrial equipment. Fund
                                    management is important for maintaining a safe industrial environment.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_03.svg" class="img-mob" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/clock-two.svg" alt="">
                                    <h4 data-i18n="home.sub_minutes">Minutes</h4>
                                </div>
                                <p data-i18n="home.text_minutes">
                                    Record, track and control meeting minutes and their actions in a simple and
                                    user-friendly manner. Simplicity and ease of use are fundamental to an efficient
                                    routine.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_04.svg" class="img-mob" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/file-check.svg" alt="">
                                    <h4 data-i18n="home.sub_p_control">Process Control</h4>
                                </div>
                                <p data-i18n="home.text_p_control">
                                    Monitor your key control and verification variables in an integrated way with your
                                    industrial systems. Stability of the process ensures better operating results.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_05.svg" class="img-mob" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-module">
                                <div class="title">
                                    <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/chart-bar.svg" alt="">
                                    <h4 data-i18n="home.sub_improvoments">Improvements</h4>
                                </div>
                                <p data-i18n="home.text_improvoments">
                                    Conduct analysis and handling of critical problems from a step by step guided cycle.
                                    The maturity of a plant is measured by the low recurrence of problems.
                                </p>
                                <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/ilustra-tela_06.svg" class="img-mob" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="s-depoiments" data-piio-bck="img/opt/bg-depo.svg">
        <div class="container" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true">
            <p data-i18n="home.text_depo">
                "Our differential goes beyond system integration. We evolve our platform listening to the real needs of
                our customers. We always deliver what we promise, adding value to the end customer. Integrate,
                cosolidate and aggregate. That is the basis of all our efforts."
            </p>
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-depo.svg" class="icon" alt="">
            <span>Rodrigo Dal Moro - Head of Product</span>
        </div>
    </section>

    <section class="s-partners section" id="clients">
        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/line-partners.svg" class="line-partners" alt="">
        <div class="container">
            <h1 data-aos="fade-down" data-aos-duration="1000" data-aos-once="true" data-i18n="home.title_partners">Who
                is with us?</h1>
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/line-4.png" class="line" alt="" data-aos="fade-down" data-aos-duration="1000"
                data-aos-once="true">
            <div class="slide-text-partners" data-aos="fade-down" data-aos-duration="1000" data-aos-once="true">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <p data-i18n="home.text_eldorado">
                            Eldorado is a company that has always grown in results, efficiency and production. Our
                            numbers are extremely competitive and, most of the time, based on AEVO Industriall
                            management. The platform is not only a technology for us; it is the basis of our industry
                            and demonstrates how we transcribe our management. As a manager of technical control and
                            processes, I consider AEVO industriall to be one of our main industrial management platforms
                            at Eldorado.
                        </p>
                        <h3>Leonardo Pimenta</h3>
                        <span data-i18n="home.span_prof_eldorado">Technical Manager</span>
                    </div>
                    <div class="swiper-slide">
                        <p data-i18n="home.text_veracel">
                            The main transformation that the AEVO Industriall brought to Veracel was the consolidation
                            of all the information in a single platform, as well as more agility in the management of
                            the information and control of production losses, factors that are extremely important for
                            the manager’s decision making process. I work with industrial systems at Veracel and should
                            devote 80% of my time to AEVO Industriall. AEVO is a very partner company, work together
                            with us and always suits the needs of the client.
                        </p>
                        <h3>André Silva Borges</h3>
                        <span data-i18n="home.span_prof_veracel">Industrial Automation Systems Analyst</span>
                    </div>
                    <div class="swiper-slide">
                        <p data-i18n="home.text_lwarcel">
                            While using the platform, Lwarcel has become more agile in making decisions. AEVO
                            Industriall has helped us in a change of culture where we have improved the quality of the
                            information we put into the system. The main benefit of AEVO Industriall is the
                            user-friendly interface that contributes to the practicality of launching occurrences in the
                            system, as well as being able to integrate some information that we used to have in separate
                            places.
                        </p>
                        <h3>Bruno Calvasan Fenara</h3>
                        <span data-i18n="home.span_prof_lwarcel">Production Coordinator</span>
                    </div>
                    <div class="swiper-slide">
                        <p data-i18n="home.text_suzano">
                            The existing works of production in the dynamic, dynamic systems in the production of the
                            production that same same dynamism. We are finding in AEVO a partner willing to customize
                            their systems to our needs, thus speeding up decision making in our processes. Decisions are
                            agile and assertive decisions that are critical to making the best results even better.
                        </p>
                        <h3>Vinícius Sierra</h3>
                        <span data-i18n="home.span_prof_suzano">Consultant of Industries Process</span>
                    </div>
                </div>
            </div>
            <div class="slide-logos" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/eldorado.svg" alt="Logo Eldorado">
                    </div>
                    <div class="swiper-slide">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/veracel.png" alt="Logo Veracel">
                    </div>
                    <div class="swiper-slide">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/bracell.png" alt="Logo Lwaercel">
                    </div>
                    <div class="swiper-slide">
                        <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/suzano.png" alt="Logo Suzano">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="s-blog section" id="news" data-piio-bck="img/opt/what-is-knew.svg">
        <div class="container">
            <span class="seal" data-aos="fade-down" data-aos-duration="1000" data-aos-once="true">blog</span>
            <h2 data-aos="fade-down" data-aos-duration="1000" data-aos-once="true" data-i18n="home.title_blog">What is
                knew? Get to know our blog
            </h2>
            <div class="all-posts" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                <div class="swiper-wrapper">
                    <?php
                        $url = 'http://blog.industriallcorp.com/feed/';
                        $xml = simplexml_load_file($url);

                        if($xml !== false){
                            foreach($xml->channel->item as $node){ 
                    ?>
                                <div class="swiper-slide">
                                    <div class="box-post">
                                        <a href="<?=$node->link?>" target="_blank">
                                            <?php
                                                $description = $node->description; 
                                                preg_match( '/src="([^"]*)"/i', $description, $array ) ;
                                                $finalStr = $array[0];
                                                $explode = explode('=', $finalStr);
                                                $img  = str_replace('"', '', $explode[1]);
                                            ?>
                                            <div class="img">
                                                <img data-piio='<?= $img  ?>' src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="">
                                            </div>                                                                             
                                            <p><?= $node->title ?></p>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                    ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>

    <footer data-piio-bck="img/opt/bg-footer.jpg">
        <div class="container">
            <img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/arrow-footer.svg" class="arrow" alt="">
            <h1 data-i18n="home.title_touch">Keep in touch</h1>
            <p data-i18n="home.text_touch">Send a message and you will hear from us as soon as possible!</p>
            <a href="contato.html" class="btn" id="contact-aevo-team" data-i18n="home.btn_touch">Contact Aevo Team</a>
            <ul>
                <li>
                    <a href="https://www.instagram.com/aevo.ti/" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/AevoTI/" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/AevoTI" target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/company/aevo-ti/" target="_blank">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UC6elpZXXa2TMa5kvrNgC-yQ"
                        target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
            </ul>
            <div class="direitos">
                <span>© AEVO - 2019. Todos os direitos reservados.</span>
                <div class="dev">
                    <span>Design by:</span>
                    <a href="https://insanydesign.com/" target="_blank"><img  src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-piio="img/opt/logo-insany.svg" alt=""></a>
                </div>
            </div>
        </div>
    </footer>

    <div class="fixed-bar">
        <ul>
            <li>
                <a href="#start">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_start">start</span>
                </a>
            </li>
            <li>
                <a href="#purpose">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_purpose">purpose</span>
                </a>
            </li>
            <li>
                <a href="#features">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_features">features</span>
                </a>
            </li>
            <li>
                <a href="#modules">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_modules">modules</span>
                </a>
            </li>
            <li>
                <a href="#clients">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_clients">clients</span>
                </a>
            </li>
            <li>
                <a href="#news">
                    <div class="circle"></div>
                    <span data-i18n="home.menu_news">blog</span>
                </a>
            </li>
        </ul>
    </div>

    <script type="application/javascript"> var piioData = { appKey: 'hj9lpq', domain: 'https://industriallcorp.com' } </script>
    <script src="//js.piio.co/hj9lpq/piio.min.js"></script>

    <script src="translate/home.js"></script>
    <script src="js/all.js"></script>

    <script>
        const menu = () => { const a = document.getElementById("js-open-menu"); a.addEventListener("click", a => { a.preventDefault(), document.documentElement.classList.toggle("menu-opened"), $(".menu-mobile").fadeToggle(), $("body").toggleClass("open-menu"), $(".menu-button").toggleClass("fixed-btn") }) }; menu();
    </script>
    <script>
    function Utils() {

    }

    Utils.prototype = {
        constructor: Utils,
        isElementInView: function (element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offset().top;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
        }
    };

    var Utils = new Utils();

    

    function playSvg(){
        var isElementInView = Utils.isElementInView($('#motion'), false);
        if (isElementInView) {
            //console.log('in view');
            motion.play();
        } else {
            //console.log('out of view');
            motion.stop();
        }
    }

    window.addEventListener("scroll", playSvg);
    </script>
    
</body>

</html>